<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();
Route::get('/home', 'HomeController@index');
Route::group(['middleware' => 'auth'], function(){
    Route::get('/profile/{slug}', [
        'uses' => 'ProfileController@index',
        'as' => 'profile'
    ]);
    Route::get('/profile/edit/profile', [
        'uses' => 'ProfileController@edit',
        'as' => 'profile.edit'
    ]);
    Route::post('/profile/update/profile', [
        'uses' => 'ProfileController@update',
        'as' => 'profile.update'
    ]);
    Route::get('/check_relationship_status/{id}', [
        'uses' => 'FriendshipController@check',
        'as' => 'check'
    ]);
    Route::get('/add_friend/{id}', [
        'uses' => 'FriendshipController@add_friend',
        'as' => 'add_friend'
    ]);
    Route::get('/accept_friend/{id}', [
        'uses' => 'FriendshipController@accept_friend',
        'as' => 'accept_friend'
    ]);
    Route::get('get_unread', function(){
        return Auth::user()->unreadNotifications;
    });
    Route::get('/notifications', [
        'uses' => 'HomeController@notifications',
        'as' => 'notifications'
    ]);
    Route::get('/feed', [
        'uses' => 'FeedController@feed',
        'as' => 'feed'
    ]);
    Route::post('/create/post', [
        'uses' => 'PostsController@store'
    ]);
    Route::get('/get_auth_user_data', function(){
        return Auth::user();
    });
    Route::get('/like/{id}', [
        'uses' => 'LikeController@like'
    ]);
    Route::get('/unlike/{id}', [
        'uses' => 'LikeController@unlike'
    ]);
    Route::post('/comment/{id}', [
        'uses' => 'CommentController@comment'
    ]);
    Route::get('/comment/{id}', [
        'uses' => 'CommentController@uncomment'
    ]);
});
