<?php
namespace App\Http\Controllers;
use Auth;
use App\Post;
use App\Comment;
use Illuminate\Http\Request;
class CommentController extends Controller
{
    public function comment($id, Request $request)
    {
        $post = Post::find($id);
        $comment =  Comment::create([
            'user_id' => Auth::id(),
            'post_id' => $post->id,
            'comment' => $request->comment
        ]);
        return Comment::find($comment->id);
    }
    public function uncomment($id)
    {
        $post = Post::find($id);

        $comment = Comment::where('user_id', Auth::id())
            ->where('post_id', $post->id)
            ->first();
        $comment->delete();
        return $comment->id;
    }
}
